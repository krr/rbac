# RBAC

## How to run:
In the rbac repository folder, run the following commands:

- ./reset.sh >>> resets and initializes the state
- ./query.sh "FO(.) query" >>> query the current state with an FO(.) query over the original vocabulary
- ./update.sh "actions" >>> update the state with new concurrent actions, written as facts over the stateless vocabulary
- ./plan.sh "FO(.) constraint" >>> interactively search for a plan achieving the desired goal constraint
- ./history.sh >>> displays all previously executed commands

## The following scripts should not be called manually:
- intern/prepareActions.sh
- intern/eraseLastActions.sh
- intern/increaseNow.sh

## Plug in your own LTC specification:
- adjust the file "idpfiles/theory.idp" with the neede knowledge
- link adjusted specifications in "idpfiles/theory.idp" with the corresponding methods in "idpfiles/methods.idp"

