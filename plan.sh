#!/bin/bash

value=`cat now`
echo "$value: plan $1" >> history

./intern/prepareActions.sh

idpfile="idpfiles/constraints.idp"

echo "" > $idpfile
echo "theory Constraints:V {" >> $idpfile
echo "$1" >> $idpfile
echo "}" >> $idpfile

./idp3-3.7.1/bin/idp idpfiles/all.idp -e "plan()"
