#!/bin/bash

value=`cat now`
echo "$value: update $1" >> history
echo "$1" | sed "s/)/,$value)/g" >> actions

./intern/prepareActions.sh

./idp3-3.7.1/bin/idp idpfiles/all.idp -e "update()"
