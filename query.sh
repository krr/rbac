#!/bin/bash

value=`cat now`
echo "$value: query $1" >> history

./intern/prepareActions.sh

idpfile="idpfiles/query.idp"

echo "" > $idpfile
echo "query CurrentQuery:V{" >> $idpfile
echo "$1" >> $idpfile
echo "}" >> $idpfile

./idp3-3.7.1/bin/idp idpfiles/all.idp -e "executeQuery()" 

./intern/increaseNow.sh
