#!/bin/bash

rm actions
touch actions
echo "0" > now
rm history
touch history

rm idpfiles/query.idp
touch idpfiles/query.idp
rm idpfiles/actions.idp
touch idpfiles/actions.idp
rm idpfiles/constraints.idp
touch idpfiles/constraints.idp

touch intern/actionstub
./idp3-3.7.1/bin/idp idpfiles/initial.idp idpfiles/methods.idp -e "generateBoilerPlate()"
